//======================================================================================
//======================================================================================
//		File:		mainwindow.cpp
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================

#include "mainwindow.h"
#include "Globals.h"

using namespace std;
MainWindow	*g_mainWindow = NULL;
//=====================================================================================
//	MainWindow::	MainWindow()
//
//	Constructor that initializes the gui main window.
//=====================================================================================

MainWindow::MainWindow	 ()
    :  QMainWindow	 (),
       m_tabInput	 (NULL),
       m_tabOutput (NULL),
       m_tabPalette(NULL),
       m_tabInfo	 (NULL),
       m_controlPanel(NULL),
       m_tabWindow	 (NULL)
{
    // set g_mainWindow
    g_mainWindow = this;

    // setup GUI with actions, menus, and widgets
    createActions();	// create actions for each menu item
    createMenus  ();	// create menus and associate actions
    createWidgets();	// create window widgets



    // set window size
    setMinimumSize(1000, 600);
    resize	      (800, 600);

    setWindowTitle("Photo Mosaic Creator v2.2");
}



//=====================================================================================
//	MainWindow::	~MainWindow()
//
//	Destroys the window when exited.
//=====================================================================================

MainWindow::~MainWindow()
{
}



//=====================================================================================
//	MainWindow ::	createActions()
//
//	Calls the functions that will create the actions for the file, edit, and view menus.
//=====================================================================================

void MainWindow::createActions()
{
    createActionsFile();
    createActionsEdit();
    createActionsView();
    createActionsTools();
}


//=====================================================================================
//	MainWindow ::	createMenus()
//
//	Calls the functions the will create the file, edit, and view, menu.
//=====================================================================================
void
MainWindow::createMenus()
{
    createMenuFile();
    createMenuView();
    createMenuEdit();
}



//=====================================================================================
//	MainWindow ::	createActionsFile()
//
//	Creates the actions for the file menu.
//=====================================================================================

void
MainWindow::createActionsFile()
{
    m_actionNewProject	=	new	QAction(tr("&New Project"), this);
    m_actionNewProject	->	setIcon(QIcon(":/icons/file-new.png"));
    m_actionNewProject	->	setShortcut(QKeySequence::New);
    connect(m_actionNewProject, SIGNAL(triggered()), this, SLOT(s_newProject()));

    m_actionOpenExample	=	new	QAction(tr("&Open Examples"), this);
    m_actionOpenExample	->	setShortcut(QKeySequence::Open);
    //connect()

    m_actionLoadProject	=	new	QAction(tr("&Load Project"), this);
    m_actionLoadProject	->	setIcon(QIcon(":/icons/file-load.png"));
    m_actionLoadProject	->	setShortcut(QKeySequence("Ctrl+L"));
    //connect()

    m_actionSave	=	new	QAction(tr("&Save"), this);
    m_actionSave	->	setIcon(QIcon(":/icons/file-save.png"));
    m_actionSave	->	setShortcut(QKeySequence::Save);
    //connect()

    m_actionSaveAs	=	new	QAction(tr("Save As..."),this);
    m_actionSaveAs	->	setIcon(QIcon("://icons/file-save.png"));
    m_actionSaveAs	->	setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
    //connect()

    m_actionSavePalette	=	new	QAction(tr("Save Pallete"),this);
    m_actionSavePalette	->	setShortcut(QKeySequence(tr("Ctrl+p")));
   // connect()

    m_actionLoadPalette	=	new	QAction(tr("Load Pallete"), this);
    m_actionLoadPalette	->	setShortcut(QKeySequence(tr("Ctrl+g")));

    m_actionExport	=	new	QAction(tr("&Export"), this);
    m_actionExport	->	setShortcut(tr("Ctrl+E"));


    m_actionQuit	=	new	QAction(tr("&Quit"), this);
    m_actionQuit	->	setShortcut(QKeySequence(tr("Ctrl+Q")));
    connect(m_actionQuit, SIGNAL(triggered()), this, SLOT(close()));
}



//=====================================================================================
//	MainWindow ::	createActionsEdit()
//
//	Creates the actions for the edit menu.
//=====================================================================================
void
MainWindow::createActionsEdit()
{
    m_actionUndo	=	new	QAction(tr("&Undo"),this);
    m_actionUndo	->	setShortcut(QKeySequence(tr("Ctrl+u")));

    m_actionRedo	=	new	QAction(tr("&Redo"),this);
    m_actionRedo	->	setShortcut(QKeySequence(tr("Ctrl+r")));

    m_actionCut		=	new	QAction(tr("Cut"),this);
    m_actionCut		->	setShortcut(QKeySequence(tr("Ctrl+x")));

    m_actionCopy	=	new	QAction(tr("&Copy"),this);
    m_actionCopy	->	setShortcut(QKeySequence(tr("Ctrl+c")));

    m_actionPaste	=	new	QAction(tr("&Paste"),this);
    m_actionPaste	->	setShortcut(QKeySequence(tr("Ctrl+v")));

    m_actionFillSelection	=	new	QAction(tr("Fill Selection"),this);
    m_actionFillSelection 	->	setShortcut(QKeySequence(tr("Ctrl+i")));

    m_actionBlendTool	=	new	QAction(tr("Blend Tool"),this);
    m_actionBlendTool	->	setShortcut(QKeySequence(tr("Ctrl+d")));

    m_actionCopyCurrentLayer	=	new	QAction(tr("Copy Current Layer"),this);
    m_actionCopyCurrentLayer	->	setShortcut(QKeySequence(tr("Ctrl+Shift+c")));

    m_actionResetAllParameters	=	new	QAction(tr("Reset &All Parameters"),this);
    m_actionResetAllParameters	->	setShortcut(QKeySequence(tr("Ctrl+a")));

    m_actionCropInput	=	new	QAction(tr("C&rop Input"),this);
    m_actionCropInput	->	setShortcut(QKeySequence(tr("Ctrl+r")));

    m_actionCropOutput	=	new	QAction(tr("Crop Output"),this);
    m_actionCropOutput	->	setShortcut(QKeySequence(tr("Ctrl+t")));
}



//=====================================================================================
//	MainWindow ::	createActionsView()
//
//	Creates the actions for the view menu.
//=====================================================================================

void
MainWindow::createActionsView()
{
    m_actionZoomIn	=	new	QAction(tr("Zoom In"),this);
    m_actionZoomIn	->	setShortcut(QKeySequence::ZoomIn);
    m_actionZoomIn	->	setIcon(QIcon(":/icons/view-zoomin.png"));

    m_actionZoomOut	=	new	QAction(tr("Zoom Out"),this);
    m_actionZoomOut	->	setShortcut(QKeySequence::ZoomOut);
    m_actionZoomOut	->	setIcon(QIcon(":/icons/view-zoomout.png"));

    m_actionFitWindow	=	new	QAction(tr("Fit Window"),this);
    m_actionFitWindow	->	setShortcut(QKeySequence(tr("Ctrl+w")));

    m_actionFullscreen	=	new	QAction(tr("&Fullscreen"),this);
    m_actionFullscreen	->	setShortcut(QKeySequence(tr("Ctrl+f")));

    m_actionToggleSections	=	new	QAction(tr("Toggle Sections"),this);
    m_actionToggleSections	->	setShortcut(QKeySequence(tr("Ctrl+b")));

    m_actionShowLayerManager	=	new	QAction(tr("View Layer Manager"),this);
    m_actionShowLayerManager	->	setShortcut(QKeySequence(tr("Ctrl+m")));
    m_actionShowLayerManager	->	setIcon(QIcon(":/icons/view-layer-manager.png"));

    m_actionInput	=	new	QAction(tr("Input"),this);
    m_actionInput	->	setShortcut(QKeySequence(tr("1")));

    m_actionOutput	=	new	QAction(tr("Output"),this);
    m_actionOutput	->	setShortcut(QKeySequence(tr("2")));

    m_actionPalette	=	new	QAction(tr("Palette"),this);
    m_actionPalette	->	setShortcut(QKeySequence(tr("3")));

    m_actionInfo	=	new	QAction(tr("Info"),this);
    m_actionInfo	->	setShortcut(QKeySequence(tr("4")));
}



//=====================================================================================
//	MainWindow ::	createActionsTools()
//
//	Creates the actions for the edit tools toolbar.
//=====================================================================================

void
MainWindow::createActionsTools()
{
    m_actionPencil	=	new	QAction(tr("Pencil"),this);
    m_actionPencil	->	setIcon(QIcon(":/icons/tool-pencil-22.png"));

    m_actionEraser	=	new	QAction(tr("Eraser"),this);
    m_actionEraser	->	setIcon(QIcon(":/icons/tool-eraser-22.png"));

    m_actionFuzzySelect	=	new QAction(tr("Fuzzy Select"),this);
    m_actionFuzzySelect	->	setIcon(QIcon(":/icons/tool-fuzzy-select-22.png"));

    m_actionColorSelect	=	new	QAction(tr("Color Select"),this);
    m_actionColorSelect	->	setIcon(QIcon(":/icons/tool-by-color-select-22.png"));

    m_actionColorPicker	=	new	QAction(tr("Color Picker"),this);
    m_actionColorPicker	->	setIcon(QIcon(":/icons/tool-color-picker-22.png"));

    m_actionRectSelect	=	new	QAction(tr("Rectangle Select"),this);
    m_actionRectSelect	->	setIcon(QIcon(":/icons/tool-rect-select-22.png"));

    m_actionNullTile	=	new	QAction(tr("Null Tile"),this);
    m_actionNullTile	->	setIcon(QIcon(":/icons/null-tile.png"));

    m_actionNone	=	new	QAction(tr("None"),this);
    m_actionNone	->	setIcon(QIcon(":/icons/tool-none-icon.png"));
}



//===========================================================================================
//	MainWindow ::	createMenuFile()
//
//	Creates the file menu.
//===========================================================================================

void
MainWindow::createMenuFile()
{
    //File Menu:
    QMenu	*menuFile	=	menuBar()	->	addMenu("&File");

    menuFile	->	addAction(m_actionNewProject);
    menuFile	->	addAction(m_actionOpenExample);
    menuFile	->	addAction(m_actionLoadProject);
    menuFile	->	addSeparator();
    menuFile	->	addAction(m_actionSave);
    menuFile	->	addAction(m_actionSaveAs);
    menuFile	->	addSeparator();
    menuFile	->	addAction(m_actionSavePalette);
    menuFile	->	addAction(m_actionLoadPalette);
    menuFile	->	addSeparator();
    menuFile	->	addAction(m_actionExport);
    menuFile	->	addSeparator();
    menuFile	->	addAction(m_actionQuit);

    //File ToolBar
    QToolBar	*toolbarFile	=	new	QToolBar;

    toolbarFile	=	addToolBar(tr("&File"));
    toolbarFile	->	addAction(m_actionNewProject);
    toolbarFile	->	addAction(m_actionLoadProject);
    toolbarFile	->	addAction(m_actionSave);
}



//===========================================================================================
//	MainWindow ::	createMenuEdit()
//
//	Creates the edit menu.
//===========================================================================================

void
MainWindow::createMenuEdit()
{
    //Edit Menu
    QMenu	*menuEdit	=	menuBar()	->	addMenu("&Edit");

    menuEdit	->	addAction(m_actionUndo);
    menuEdit	->	addAction(m_actionRedo);
    menuEdit	->	addSeparator();
    menuEdit	->	addAction(m_actionCut);
    menuEdit	->	addAction(m_actionCopy);
    menuEdit	->	addAction(m_actionPaste);
    menuEdit	->	addAction(m_actionFillSelection);
    menuEdit	->	addAction(m_actionBlendTool);
    menuEdit	->	addAction(m_actionCopyCurrentLayer);
    menuEdit	->	addSeparator();
    menuEdit	->	addAction(m_actionResetAllParameters);
    menuEdit	->	addSeparator();
    menuEdit	->	addAction(m_actionCropInput);
    menuEdit	->	addAction(m_actionCropOutput);

    //Edit Tools Toolbar
    QToolBar	*toolbarEdit	=	new	QToolBar;

    toolbarEdit	=	addToolBar(tr("Edit"));
    toolbarEdit	->	addAction(m_actionPencil);
    toolbarEdit	->	addAction(m_actionEraser);
    toolbarEdit	->	addAction(m_actionFuzzySelect);
    toolbarEdit	->	addAction(m_actionColorSelect);
    toolbarEdit	->	addAction(m_actionColorPicker);
    toolbarEdit	->	addAction(m_actionRectSelect);
    toolbarEdit	->	addSeparator();
    toolbarEdit	->	addAction(m_actionNullTile);
    toolbarEdit	->	addAction(m_actionNone);
}



//===========================================================================================
//	MainWindow ::	createMenuView()
//
//	Creates the view menu.
//===========================================================================================

void
MainWindow::createMenuView()
{
    //View Menu
    QMenu	*menuView	=	menuBar()	->	addMenu("&View");

    menuView	->	addAction(m_actionZoomIn);
    menuView	->	addAction(m_actionZoomOut);
    menuView	->	addAction(m_actionFitWindow);
    menuView	->	addAction(m_actionFullscreen);
    menuView	->	addSeparator();
    menuView	->	addAction(m_actionToggleSections);
    menuView	->	addAction(m_actionShowLayerManager);
    menuView	->	addSeparator();
    menuView	->	addAction(m_actionInput);
    menuView	->	addAction(m_actionOutput);
    menuView	->	addAction(m_actionPalette);
    menuView	->	addAction(m_actionInfo);

    //View ToolBar
    QToolBar	*toolbarView	=	new	QToolBar;

    toolbarView	=	addToolBar(tr("&View"));
    toolbarView	->	addAction(m_actionZoomIn);
    toolbarView	->	addAction(m_actionZoomOut);
    toolbarView	->	addAction(m_actionShowLayerManager);
}



//===========================================================================================
//	MainWindow ::	createWidgets()
//
//	Calls the functions that will create the tabs and control panel toolbox.
//===========================================================================================

void
MainWindow::createWidgets()
{
       createTab();

       m_controlPanel	=	new	controlPanel;
       m_controlPanel	->	setMinimumSize(350, QSizePolicy::Expanding);

       QHBoxLayout  *hbox=new QHBoxLayout();
       hbox->addWidget(m_tabWindow);
       hbox->addWidget(m_controlPanel);
       hbox->setContentsMargins(0,0,0,0);

       //set central widget
       QWidget  *w = new QWidget;
       w->setLayout(hbox);
       setCentralWidget(w);
       w->setContentsMargins(0,0,0,0);

}



//===========================================================================================
//	MainWindow ::	createTab()
//
//	Creates a tab window with an input, output, palette, and info tabs.
//===========================================================================================

void
MainWindow::createTab()
{
    m_tabWindow		=	new	QTabWidget;

    m_tabInput		=	new	ImageWindow;
    m_tabOutput		=	new	QWidget;
    m_tabPalette	=	new	QWidget;
    m_tabInfo		=	new	QWidget;

    m_tabWindow	->	addTab(m_tabInput, "Input");
    m_tabWindow	->	addTab(m_tabOutput, "Output");
    m_tabWindow	->	addTab(m_tabPalette, "Pallete");
    m_tabWindow	->	addTab(m_tabInfo, "Info");

    m_tabWindow	->	setMinimumSize(600,600);
    m_tabWindow	->	setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
}



//===========================================================================================


void
MainWindow::updateInputFrame()
{
    m_tabInput->setImage(m_params.image());
}



TesseraParameters&
MainWindow::parameters()
{
    return m_params;
}



// Slot functions
void
MainWindow::s_newProject()
{
    // prompt for input filename
    QFileDialog dialog(this);

    // open the last known working directory
    if(!m_currentInDir.isEmpty())
        dialog.setDirectory(m_currentInDir);

    // display existing files and directories
    dialog.setFileMode(QFileDialog::ExistingFile);

    // invoke native file browser to select file
    QString sel("Images");
    QString file =  dialog.getOpenFileName(this,
                 "Open File", m_currentInDir,
                 "Images (*.jpg *.jpeg *.png *.bmp *.tiff *.tif);;"
                  "All files (*)", &sel);

    // no file selected.. return
    if(file.isNull())
        return;

    // update location of current directory
    m_currentInDir = QFileInfo(file).dir().absolutePath();

    // load input image
    QImage image = QImage(file);

    // check whether image could be read
    if(image.isNull()) {
        QMessageBox::critical(this, tr ("Load Image Error"), tr ("Cannot load image: '%1'").arg (file));
        return;
    }
    m_params.reset();
    m_params.setOriginalImage(image);
    m_params.setImage(image);
    m_controlPanel->resetControls();
    updateInputFrame();
}



void MainWindow::s_loadProject()	{}
void MainWindow::s_saveProject()	{}
void MainWindow::s_undo()		{}
void MainWindow::s_redo()		{}
void MainWindow::s_zoomIn()		{}
void MainWindow::s_zoomOut()		{}

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// MainWindow::showInputTab:
// MainWindow::showOutputTab:
// MainWindow::showPaletteTab:
// MainWindow::showInfoTab:
//
// Make the input/output/palette/info page current in the tab widget.
//
void MainWindow::s_showInputTab()  {m_tabWindow->setCurrentIndex(0);}
void MainWindow::s_showOutputTab() {m_tabWindow->setCurrentIndex(1);}
void MainWindow::s_showPaletteTab(){m_tabWindow->setCurrentIndex(2);}
void MainWindow::s_showInfoTab()   {m_tabWindow->setCurrentIndex(3);}


