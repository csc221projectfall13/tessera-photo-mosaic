//======================================================================================
//======================================================================================
//		File:		main.cpp
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================

#include "mainwindow.h"
#include <QApplication>


int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    MainWindow w;

    w.show();

    return a.exec();
}


