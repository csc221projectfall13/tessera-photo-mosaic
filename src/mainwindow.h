//======================================================================================
//======================================================================================
//		File:		mainwindow.h
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include	"controlPanel.h"

#include    <QtGui>
#include    <QtWidgets>
#include	<QMainWindow>
#include	<QMenu>
#include	<QMenuBar>
#include	<QToolBar>
#include	<QToolBox>
#include	<QVBoxLayout>
#include	<QHBoxLayout>
#include	<QRadioButton>
#include	<QSlider>
#include	<QPushButton>
#include	<QSpinBox>
#include	<QAction>
#include	<QSplitter>
#include "TesseraParameters.h"
#include "controlPanel.h"
#include "ImageWindow.h"

class
MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    //! Constructor.
    MainWindow();

    TesseraParameters&	parameters		();
    void			updateInputFrame	();

    //! Destructor.
    ~MainWindow();
public slots:
    // slots
    void s_newProject	();
    void s_loadProject	();
    void s_saveProject	();
    void s_undo		();
    void s_redo		();
    void s_zoomIn		();
    void s_zoomOut		();
    void s_showInputTab	();
    void s_showOutputTab	();
    void s_showPaletteTab	();
    void s_showInfoTab	();

private:

    // create widgets
    void	createWidgets		();

    // create actions
    void	createActions		();
    void	createActionsFile	();
    void	createActionsEdit	();
    void	createActionsView	();
    void	createActionsTools	();

    // create menus
    void	createMenus			();
    void	createMenuFile		();
    void	createMenuEdit		();
    void	createMenuView		();


    void	createToolBox		();
    void	createInputSettings	();
    void	createMosaicSize	();

    void	createTab			();

    // actions
    QAction		*m_actionNewProject;
    QAction		*m_actionOpenExample;
    QAction		*m_actionLoadProject;
    QAction		*m_actionSave;
    QAction		*m_actionSaveAs;
    QAction		*m_actionSavePalette;
    QAction		*m_actionLoadPalette;
    QAction		*m_actionExport;
    QAction		*m_actionQuit;

    QAction		*m_actionUndo;
    QAction		*m_actionRedo;
    QAction		*m_actionCut;
    QAction		*m_actionCopy;
    QAction		*m_actionPaste;
    QAction		*m_actionFillSelection;
    QAction		*m_actionBlendTool;
    QAction		*m_actionCopyCurrentLayer;
    QAction		*m_actionResetAllParameters;
    QAction		*m_actionCropInput;
    QAction		*m_actionCropOutput;

    QAction		*m_actionZoomIn;
    QAction		*m_actionZoomOut;
    QAction		*m_actionFitWindow;
    QAction		*m_actionFullscreen;
    QAction		*m_actionToggleSections;
    QAction		*m_actionShowLayerManager;
    QAction		*m_actionInput;
    QAction		*m_actionOutput;
    QAction		*m_actionPalette;
    QAction		*m_actionInfo;

    QAction		*m_actionPencil;
    QAction		*m_actionEraser;
    QAction		*m_actionFuzzySelect;
    QAction		*m_actionColorSelect;
    QAction		*m_actionColorPicker;
    QAction		*m_actionRectSelect;
    QAction		*m_actionNullTile;
    QAction		*m_actionNone;



    // widgets
    ImageWindow		*m_tabInput;
    QWidget		*m_tabOutput;
    QWidget		*m_tabPalette;
    QWidget		*m_tabInfo;

    controlPanel		*m_controlPanel;
    QTabWidget	*m_tabWindow;

    // other stuff
    QString		  m_currentInDir;
    TesseraParameters m_params;
};

#endif // MAINWINDOW_H

