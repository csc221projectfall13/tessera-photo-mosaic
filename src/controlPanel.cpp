//======================================================================================
//======================================================================================
//		File:		controlPanel.cpp
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================


#include	"controlPanel.h"
#include	"mainwindow.h"
#include "Globals.h"
#include "HSL.h"



//===========================================================================================
//	controlPanel ::	controlPanel()
//
//	Constructor: Calls the functions to create the control panel and setup it's layout.
//===========================================================================================


controlPanel::controlPanel(QWidget	*parent, Qt::WindowFlags f)	:	QWidget(parent, f)
{
    createPanelTree();

    QVBoxLayout	 *vbox	=	new QVBoxLayout;
    vbox	->	addWidget(m_tree);
    setLayout(vbox);

}



//===========================================================================================
//	controlPanel ::	createPanelTree()
//
//	Creates the tree for the control panel.
//===========================================================================================


void
controlPanel::createPanelTree()
{
    m_tree	= 	new QTreeWidget();
    m_tree	->	setHeaderHidden(true); //hide tree header
    m_tree  ->  setColumnCount(1);      //one-column tree
    m_tree  ->  setIndentation(10); //children indentation
    m_tree  ->  setAnimated(true);  //animate expand / collapse
    m_tree  ->  setSelectionMode(QAbstractItemView::NoSelection);
    m_tree  ->  setFocusPolicy(Qt::NoFocus);
    m_tree  ->  setFixedWidth(350);
    m_tree	->	setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Expanding);

    // create control panel groupboxes and store in m_group
    m_group.push_back(createGroupInput());
    m_group.push_back(createGroupMosaicSize());
    m_group.push_back(createGroupMosaicRendering());
    m_group.push_back(createGroupTilePalette());
    m_group.push_back(createGroupGrout());

    // init font and string list (name) for control panel tree widget
    QFont font("Arial",11, QFont::Bold);

    QStringList	name;

    name	<<	"Input	Settings"
            <<	"Mosaic	Size"
            <<	"Mosaic Rendering"
            <<	"Tile	Palette"
            <<	"Grout";


    for(int i=0; i<m_group.size(); ++i)
    {
        // make control panel header and widget
        m_header.push_back(new QTreeWidgetItem(m_tree));
        m_item.push_back(new QTreeWidgetItem(m_header[i]));

        // make pushbutton to expand/collapse control panel;
        m_button.push_back(new QPushButton(name[i]));
        m_button[i] ->  setFont(font);
        m_button[i] ->  setFixedWidth(330);
        m_button[i]	->	setFlat(true);
        m_button[i]	->	setStyleSheet("Text-align:left");

        // set the header item to be a pushbutton
        m_tree	->	setItemWidget(m_header[i],0,m_button[i]);

        // set the widget item to be a groupbox
        m_group[i]->setFixedWidth((320));
        m_tree	->	setItemWidget(m_item[i],0,m_group[i]);

        // init signal/slot connections to allow
        // expansion/collapse when headers are pressed
         connect(m_button[i], SIGNAL(pressed()),
         this,	     SLOT  (expandPanels()));
    }
    // start up with some expanded panels
    m_header[0]->setExpanded(true);
}



//===========================================================================================
//	controlPanel ::	createGroupInput()
//
//	Creates the input settings groupbox for the control panel.
//===========================================================================================


QGroupBox*
controlPanel::createGroupInput()
{
    // init group box
    QGroupBox	*groupboxInput	=	new	QGroupBox;

    // create reset pushbuttons
    for(int	i=0; i<5; i++)
    {
        m_pushbuttonInputReset[i]	=	new	QPushButton(tr("Reset"));
    }

    // pushbutton for resetting all parameters


    QPushButton *m_pushbuttonRevertToOriginal	=	new	QPushButton(tr("Revert To Original"));

    // init slider labels
    QLabel	*label[5];
    label[0]	=	new QLabel(tr("Brightness"));
    label[1]	=	new	QLabel(tr("Contrast"));
    label[2]	=	new	QLabel(tr("Hue"));
    label[3]	=	new	QLabel(tr("Saturation"));
    label[4]	=	new	QLabel(tr("Lightness"));

    // create brightness/contrast sliders
    for(int	i=0; i<5; i++)
    {
       // create slider and init value
        m_sliderInput[i]	=	new	QSlider(Qt::Horizontal);
        m_sliderInput[i]	->	setValue(0);


        // set range of slider values
        switch(i) {
        case 0: m_sliderInput[i]->setRange(-128, 128); break;	// brightness
        case 1: m_sliderInput[i]->setRange(-100, 100); break;	// contrast
        case 2: m_sliderInput[i]->setRange(-180, 180); break;	// hue
        case 3: m_sliderInput[i]->setRange(-100, 100); break;	// saturation
        case 4: m_sliderInput[i]->setRange(-100, 100); break;	// lightness
         }
        // init label for slider value
        m_label[i]	=	new	QLabel(QString("%1").arg(0,5));

        // set width of reset buttons
        m_pushbuttonInputReset[i]->setFixedWidth(70);
    }


    // create brightness/contrast widget
     QWidget    *bcWidget = new QWidget;

     // assemble widgets in grid
     QGridLayout	*gridlayoutBrightnessContrast	=	new	QGridLayout;

     gridlayoutBrightnessContrast->setColumnMinimumWidth(2,35);

     for(int i=0; i<2; i++) {
         gridlayoutBrightnessContrast	->  addWidget(label[i], i, 0);
         gridlayoutBrightnessContrast	->	addWidget(m_sliderInput[i],i,1);
         gridlayoutBrightnessContrast	->	addWidget(m_label[i],i,2);
         gridlayoutBrightnessContrast	->	addWidget(m_pushbuttonInputReset[i],i,3);
         }


     bcWidget->setLayout(gridlayoutBrightnessContrast);

     // create hue/saturation widget
     QWidget	*hslWidget = new QWidget;

     // assemble widgets in grid
     QGridLayout	*gridlayoutHueSaturation	=	new	QGridLayout;

     gridlayoutHueSaturation->setColumnMinimumWidth(2,35);

     for(int i=2; i<5; i++) {
         gridlayoutHueSaturation	->  addWidget(label[i], i, 0);
         gridlayoutHueSaturation	->	addWidget(m_sliderInput[i],i,1);
         gridlayoutHueSaturation	->	addWidget(m_label[i],i,2);
         gridlayoutHueSaturation	->	addWidget(m_pushbuttonInputReset[i],i,3);
         }


     hslWidget->setLayout(gridlayoutHueSaturation);

     // create tab widget
     m_stackWidgetIn = new QStackedWidget;
 //    QTabWidget *tabWidget =new QTabWidget;

     bcWidget->setContentsMargins(0,0,0,0);
     hslWidget->setContentsMargins(0,0,0,0);
     gridlayoutBrightnessContrast->setContentsMargins(0,0,0,0);
     gridlayoutHueSaturation->setContentsMargins(0,0,0,0);
     m_stackWidgetIn->setContentsMargins(0,0,0,0);
     QVBoxLayout    *vbox = new QVBoxLayout;

    // create radio buttons for switching between RGB and HSL
     m_radioInput[0]	=	new	QRadioButton(tr("Brightness/Contrast"));
     m_radioInput[1]		=	new	QRadioButton(tr("Hue/Saturation"));
     QFont font("Arial",11,QFont::Normal);
     m_radioInput[0]->setFont(font);
     m_radioInput[1]->setFont(font);

    // create button group and insert the two radio buttons into it
    QButtonGroup	*buttongroupInput	=	new	QButtonGroup;
    buttongroupInput	->	addButton(m_radioInput[0],0);
    buttongroupInput	->	addButton(m_radioInput[1],1);

    m_radioInput[0]->setChecked(true);
    QWidget *w = new QWidget;

    // add both radio buttons into a widget having a horizontal layout
    QHBoxLayout *hbox = new QHBoxLayout;
    w->setContentsMargins(0,0,0,0);
    hbox->setContentsMargins(0,0,0,0);
    vbox->setContentsMargins(0,0,0,0);
    hbox->addWidget(m_radioInput[0]);
    hbox->addWidget(m_radioInput[1]);

    // add radio buttons, stack widget, and revertOriginal pushbutton into a vbox layout
    w->setLayout(hbox);
    vbox->addWidget(w);
    vbox->addWidget(m_stackWidgetIn);
    vbox->addWidget(m_pushbuttonRevertToOriginal);


 //   tabWidget->addTab(bcWidget,tr("Brightness/Contrast"));
 //   tabWidget->addTab(hslWidget,tr("Hue/Saturation"));

    m_stackWidgetIn->addWidget(bcWidget);
    m_stackWidgetIn->addWidget(hslWidget);



    groupboxInput	->	setLayout(vbox);

    // init signal/slot connections for brightness contrast controls
    //RGB connections
    connect(buttongroupInput,   SIGNAL(buttonClicked(int)),
            m_stackWidgetIn,SLOT    (setCurrentIndex(int)));

    connect(m_pushbuttonInputReset[0],   SIGNAL(clicked()),
            this,           SLOT    (resetBrightness()));
    connect(m_pushbuttonInputReset[1],   SIGNAL(clicked()),
            this,           SLOT    (resetContrast()));

    connect(m_sliderInput[0],   SIGNAL(valueChanged(int)),
            this,           SLOT    (changeBrightness(int)));
    connect(m_sliderInput[1],   SIGNAL(valueChanged(int)),
            this,           SLOT    (changeContrast(int)));

    connect(m_pushbuttonRevertToOriginal,   SIGNAL(clicked()),
            this,           SLOT    (revertOriginal()));

    //HSL Connection
    connect(m_pushbuttonInputReset[2],   SIGNAL(clicked()),
            this,           SLOT    (resetHue()));
    connect(m_pushbuttonInputReset[3],   SIGNAL(clicked()),
            this,           SLOT    (resetSaturation()));

    connect(m_pushbuttonInputReset[4],   SIGNAL(clicked()),
            this,           SLOT    (resetLightness()));

    connect(m_sliderInput[2],   SIGNAL(valueChanged(int)),
            this,           SLOT    (changeHue(int)));
    connect(m_sliderInput[3],   SIGNAL(valueChanged(int)),
            this,           SLOT    (changeSaturation(int)));
    connect(m_sliderInput[4],   SIGNAL(valueChanged(int)),
            this,           SLOT    (changeLightness(int)));




    return groupboxInput;
}



//===========================================================================================
//	controlPanel ::	createGroupMosaicSize()
//
//	Creates the groupbox for the mosaic size groupbox.
//===========================================================================================

QGroupBox*
controlPanel::createGroupMosaicSize()
{
    QGroupBox	*groupboxMosaicSize	=	new QGroupBox;
    QVBoxLayout *vbox =new QVBoxLayout;
    vbox->addWidget(new QPushButton("Empty"));
    groupboxMosaicSize->setLayout(vbox);
    return groupboxMosaicSize;

}




//===========================================================================================
//	controlPanel ::	createGroupMosaicRendering()
//
//	Creates the groupbox for the mosaic rendering settings.
//===========================================================================================


QGroupBox*
controlPanel::createGroupMosaicRendering()
{
    QGroupBox	*groupboxMosaicRendering	=	new QGroupBox;

    return	groupboxMosaicRendering;
}




//===========================================================================================
//	controlPanel ::	createGroupTilePalette()
//
//	Creates the groupbox for the tile palette settings.
//===========================================================================================


QGroupBox*
controlPanel::createGroupTilePalette()
{
    QGroupBox	*groupboxTilePalette	=	new QGroupBox;
    return	groupboxTilePalette;
}




//===========================================================================================
//	controlPanel ::	createGroupGrout()
//
//	Creates the groupbox for the Mosaic Size.
//===========================================================================================


QGroupBox*
controlPanel::createGroupGrout()
{
    QGroupBox	*groupboxGrout	=	new QGroupBox;
    return	groupboxGrout;
}

void
controlPanel::expandPanels()
{
    for(int i=0; i<m_group.size(); i++) {
        if(m_button[i]->isDown()) {
        if(m_header[i]->isExpanded())
            m_header[i]->setExpanded(false);
        else	m_header[i]->setExpanded(true);
        }
    }
}



// ============================================================
// BC/HSL related slots

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::resetBrightness:
//
// Set brightness to 0
//
void
controlPanel::resetBrightness()
{
    changeBrightness(0);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::resetContrast:
//
// Set contrast to 0
//
void
controlPanel::resetContrast()
{
    changeContrast(0);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::resetHue:
//
// Set hue to 0
//
void
controlPanel::resetHue()
{
    changeHue(0);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::resetSaturation:
//
// Set saturation to 0
//
void
controlPanel::resetSaturation()
{
    changeSaturation(0);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::resetLightness:
//
// Set lightness to 0
//
void
controlPanel::resetLightness()
{
    changeLightness(0);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::changeBrightness:
//
// Change brightness to user-specified value
//
void
controlPanel::changeBrightness(int val)
{
    blockSignals(true);

    m_sliderInput[0]->setValue(val);
    m_label[0]->setText(QString::number(val));
    g_mainWindow->parameters().setBrightness(val);
    updateInputImage(TesseraParameters::RGB);

    blockSignals(false);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::changeContrast:
//
// Change contrast to user-specified value
//
void
controlPanel::changeContrast(int val)
{
    blockSignals(true);

    m_sliderInput[1]->setValue(val);
    m_label[1]->setText(QString::number(val));
    g_mainWindow->parameters().setContrast(val);
    updateInputImage(TesseraParameters::RGB);

    blockSignals(false);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::changeHue:
//
// Change hue to user-specified value
//
void
controlPanel::changeHue(int val)
{
    blockSignals(true);

    m_sliderInput[2]->setValue(val);
    m_label[2]->setText(QString::number(val));
    g_mainWindow->parameters().setHue(val);
    updateInputImage(TesseraParameters::HSL);

    blockSignals(false);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::changeSaturation:
//
// Change saturation to user-specified value
//
void
controlPanel::changeSaturation(int val)
{
    blockSignals(true);

    m_sliderInput[3]->setValue(val);
    m_label[3]->setText(QString::number(val));
    g_mainWindow->parameters().setSaturation(val);
    updateInputImage(TesseraParameters::HSL);

    blockSignals(false);
}



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// ControlPanel::changeLightness:
//
// Change lightness to user-specified value
//
void
controlPanel::changeLightness(int val)
{
    blockSignals(true);

    m_sliderInput[4]->setValue(val);
    m_label[4]->setText(QString::number(val));
    g_mainWindow->parameters().setLightness(val);
    updateInputImage(TesseraParameters::HSL);

    blockSignals(false);
}



void
controlPanel::resetImage()
{
    m_image = QImage();
}



void
controlPanel::resetControls()
{
    resetInputControls();
}



void
controlPanel::resetInputControls()
{
    blockSignals(true);

    resetImage();
    for(int i=0; i<5; ++i) {
        m_sliderInput[i]->setValue(0);
        m_label[i]->setText("0");
    }

    blockSignals(false);
}



void
controlPanel::revertOriginal()
{
    // error checking
    TesseraParameters &params = g_mainWindow->parameters();
    const QImage &origImage = params.originalImage();
    const QImage &curImage = params.image();
    if(origImage.isNull() || curImage.isNull())
        return;

    g_mainWindow->parameters().setBrightness(0);
    g_mainWindow->parameters().setContrast	(0);
    g_mainWindow->parameters().setHue	(0);
    g_mainWindow->parameters().setSaturation(0);
    g_mainWindow->parameters().setLightness	(0);
    resetInputControls();

    m_image = origImage;
    params.setImage(m_image);
    g_mainWindow->updateInputFrame();
}



int
controlPanel::updateInputImage(TesseraParameters::ColorMode mode)
{
    // error checking
    TesseraParameters &params = g_mainWindow->parameters();
    const QImage &origImage = params.originalImage();
    const QImage &curImage = params.image();
    if(origImage.isNull() || curImage.isNull())
        return 0;

    if(m_image.isNull())
        m_image = origImage;

    // update colormode and current image
    if(params.colorMode() != mode)
        m_image = curImage;
    params.setColorMode(mode);

    QImage inImage = m_image;
    QImage outImage;
    if(params.colorMode() == TesseraParameters::RGB) {// brightness-contrast
        // get contrast and brightness
        int	contrast	= params.contrast();
        int	brightness	= params.brightness();

        // init slope of intensity ramp
        double	c;
        if(contrast >= 0)
            c = contrast/25. + 1.0;   // slope: 1 to 5
        else	c = 1. + (contrast/133.); // slope: 1 to 0

        // init lookup table: multiply by contrast; add brightness
        int lut[256];
        for(int v=0; v<256; v++) {
            int i = (v - 128)*c + (128 + brightness);
            lut[v] = CLIP(i, 0, 255);
        }

        // init input dimensions
        int w = inImage.width ();
        int h = inImage.height();

        // create output image
        outImage = QImage(w, h, QImage::Format_RGB32);

        // apply lookup table to source image to make input image
        for(int y=0; y<h; y++) {
            const QRgb *src = (const QRgb*) inImage.scanLine(y);
            QRgb *out  = (QRgb*) outImage.scanLine(y);
            for(int x=0; x<w; x++) {
                // set transparent pixels to white
                if(qAlpha(src[x]) < 128) {
                    *out++ = qRgb(255, 255, 255);
                } else {
                    *out++ = qRgb(lut[qRed  (src[x])],
                              lut[qGreen(src[x])],
                              lut[qBlue (src[x])]);
                }
            }
        }
    } else {			// hue-saturation
        double h = params.hue() / 180.0;
        double s = params.saturation() / 100.0;
        double l = params.lightness() / 100.0;
        HSL hsl;
        hsl.setHue	 (HSL::AllHues, h);
        hsl.setSaturation(HSL::AllHues, s);
        hsl.setLightness (HSL::AllHues, l);
        hsl.adjustHueSaturation(inImage, outImage);
    }
    params.setImage(outImage);
    g_mainWindow->updateInputFrame();

    return 1;
}
