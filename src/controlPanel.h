//======================================================================================
//======================================================================================
//		File:		controlPanel.h
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================


#ifndef	CONTROLPANEL_H
#define	CONTROLPANEL_H

#include    <QObject>
#include	<QGroupBox>
#include	<QVector>
#include	<QTreeWidget>
#include	<QTreeWidgetItem>
#include	<QPushButton>
#include	<QRadioButton>
#include	<QLabel>
#include	<QSlider>
#include	<QButtonGroup>
#include    <QtGui>
#include    <QtWidgets>
#include "TesseraParameters.h"


class
controlPanel	:	public	QWidget
{
    Q_OBJECT

    public :

        controlPanel(QWidget *parent=0,	Qt::WindowFlags	f=0);
        void        resetImage  ();

    public slots:
        void			expandPanels		();

        // BC/HSL slots
        void			resetBrightness		();
        void			resetContrast		();
        void			resetHue		();
        void			resetSaturation		();
        void			resetLightness		();
        void			changeBrightness	(int);
        void			changeContrast		(int);
        void			changeHue		(int);
        void			changeSaturation	(int);
        void			changeLightness		(int);
        void			revertOriginal		();
        void			resetInputControls	();
        void			resetControls		();


    protected :

        void	createPanelTree						();

        QGroupBox	*createGroupInput				();
        QGroupBox	*createGroupMosaicSize			();
        QGroupBox	*createGroupMosaicRendering		();
        QGroupBox	*createGroupTilePalette			();
        QGroupBox	*createGroupGrout				();


        int		updateInputImage (TesseraParameters::ColorMode mode);
    private :

        QTreeWidget*				m_tree;

        QVector	<QGroupBox*>		m_group;
        QVector	<QTreeWidgetItem*>	m_header;
        QVector	<QTreeWidgetItem*>	m_item;
        QVector	<QPushButton*>		m_button;

        // widgets for input groupbox
        QRadioButton	*m_radioInput[2];

        QPushButton		*m_pushbuttonInputReset[5];

        QLabel			*m_label[5];

        QSlider			*m_sliderInput[5];

        QStackedWidget	*m_stackWidgetIn;

        // other variables
        QImage		m_image; // image that is used as original
                         // when switching between RGB and HSL
                         // color modes

};

#endif

