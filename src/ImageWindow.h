//======================================================================================
//======================================================================================
//		File:		ImageWindow.h
//
//		Authors:	Mong Kyaw Than,	Jinying Chen, Tomasz Ksepka
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	October	24,	2013
//
//======================================================================================
//======================================================================================

#ifndef IMAGEWINDOW_H
#define IMAGEWINDOW_H

#include <QtGui>
#include <QtWidgets>

class ImageWindow : public QWidget {
    Q_OBJECT
public:
    ImageWindow(QWidget *parent=0, Qt::WindowFlags f=0);

    void	setImage	(const QImage &image);

protected:
    void	resizeEvent	(QResizeEvent *event);
    void	updatePixmap	();

    QPixmap			m_pixmap;
    QLabel		    *m_imageHolder;
private:
};

#endif	// IMAGEWINDOW_H
