//======================================================================================
//======================================================================================
//		File:		hw3.cpp
//
//		Authors:	Jinying Chen, Tomasz Ksepka, Mong Kyaw Than
//
//		Developed:	Fall 2013
//
//		Course:		CSC	221 :	Software Design Sec: R
//
//		Due Date:	November 5, 2013
//
//======================================================================================
//======================================================================================
/*
#include <QApplication>
#include <QWidget>
#include <QImage>
#include <iostream>
#include <QDebug>
#include <QGraphicsScene>
#include <QGraphicsView>

using namespace std;

int main(int argc, char *argv[])
{
    QApplication app(argc,argv);

    QGraphicsView *view = new QGraphicsView();
    QGraphicsScene *scene = new QGraphicsScene();
    view->setScene(scene);

    //Load the image from Qrc
    QImage image = QImage("://sample.jpg");

    //Get the width and height of the image
    int w = image.width();
    int h = image.height();

    cout << "Width  is: " << w <<endl;
    cout << "Height is: " << h <<endl;

    //Create Qimage for the output image
    QImage outImage = QImage(w/2,h/2,QImage::Format_RGB32);

    //Create Qimage by using Scaled function in Qt for comparasion
    QImage avgImage = image.scaled(w/2,h/2,Qt::IgnoreAspectRatio,
                               Qt::SmoothTransformation);


    QRgb tempa;
    QRgb tempb;
    QRgb tempc;
    QRgb tempd;
    QRgb tempavg;

    //Goes from first row of image
    for (int y= 0; y<h-1; y+= 2)
    {
      //Get the RGB data of first row and second row for the 2X2 subregion
      QRgb* firstrow = (QRgb*)image.scanLine(y);
      QRgb* secondrow = (QRgb*)image.scanLine(y+1);

      //Get the RGB information of the first row for the scaled image by qt
      QRgb* avg = (QRgb*)avgImage.scanLine(y/2);

      //Goes from first column
      for (int x= 0;x<w-1;x+= 2)
      {
        //Get the RGB imformation of each pixel in the 2x2 subregion
        tempa = firstrow[x];
        tempb = firstrow[x+1];
        tempc = secondrow[x];
        tempd = secondrow[x+1];
        tempavg = avg[x/2];

        //Compuet the average RGB imformation in the subregion
        int r =(qRed(tempa) + qRed(tempb) + qRed(tempc) + qRed(tempd))/4;
        int g =(qGreen(tempa)+qGreen(tempb)+qGreen(tempc)+qGreen(tempd))/4;
        int bl = (qBlue(tempa)+qBlue(tempb)+qBlue(tempc)+qBlue(tempd))/4;

        //Only print the first 5 subregion in the first row
        if ((y<2) &&(x<9))
        {

            cout <<"("<<qRed(tempa)<<","<<qGreen(tempa)<<","<<qBlue(tempa)<<")"<<","
                 <<"("<<qRed(tempb)<<","<<qGreen(tempb)<<","<<qBlue(tempc)<<")"<<","
                 <<"("<<qRed(tempc)<<","<<qGreen(tempc)<<","<<qBlue(tempc)<<")"<<","
                 <<"("<<qRed(tempd)<<","<<qGreen(tempd)<<","<<qBlue(tempd)<<")"<<":"
                 <<"("<<r<<","<<g<<","<<bl<<")"<<endl;
            cout <<"Qt scaled values are: "<<"("<<qRed(tempavg)<<","<<qGreen(tempavg)<<","<<qBlue(tempavg)<<")\n"<<endl;
        }

        //Add pixel imformation to the output image to form a new output image
        outImage.setPixel((x/2),(y/2),qRgb(r,g,bl));

      }

    }
    //Print out the width and heigt of the output image we created manually
    cout <<"The Width of output image is: "<<outImage.width()<<"  Height is: "<<outImage.height()<<endl;
    QPixmap p = QPixmap::fromImage(outImage);
    scene->addPixmap(p);
    //save the output image
    p.save("out.jpg","JPG",100);
    //show the output image created by us
    view->show();
    return app.exec();
}



*/
